#!/bin/bash

export CLASSPATH="$LANG_PATH/dist"

################################################################################

ronin_require_java () {
    echo hello world >/dev/null
}

ronin_include_java () {
    motd_text "    -> Java    : "$CLASSPATH
}

################################################################################

ronin_setup_java () {
    echo hello world >/dev/null
}

